package services

import (
	"go-mvc/domain"
	"go-mvc/utils"
)

type usersService struct{}

type usersServiceInterface interface {
	GetUser(int64) (*domain.User, *utils.ApplicationError)
	GetAllUsers() []*domain.User
	SaveUser(user domain.User) *domain.User
}

var (
	// UsersService Service to be exposed for outside the package
	UsersService usersServiceInterface
)

func init() {
	UsersService = &usersService{}
}

func (u *usersService) GetUser(userID int64) (*domain.User, *utils.ApplicationError) {
	user, err := domain.UserDao.GetUser(userID)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (u *usersService) GetAllUsers() []*domain.User {
	users := domain.UserDao.GetAllUsers()
	return users
}

func (u *usersService) SaveUser(user domain.User) *domain.User {
	savedUser := domain.UserDao.SaveUser(user)
	return savedUser
}

package controllers

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"go-mvc/domain"
	"go-mvc/services"
	"go-mvc/utils"
)

// GetUser fetches a user by its id
func GetUser(c *gin.Context) {
	userID, err := strconv.ParseInt(c.Param("user_id"), 10, 64)
	if err != nil {
		apiErr := &utils.ApplicationError{
			Message:    "user_id must be a number",
			StatusCode: http.StatusBadRequest,
			Code:       "bad_request",
		}
		utils.RespondError(c, apiErr)
		return
	}

	user, apiErr := services.UsersService.GetUser(userID)
	if apiErr != nil {
		utils.RespondError(c, apiErr)
		return
	}

	utils.Respond(c, http.StatusOK, user)
}

// GetAllUsers fetches all users from database
func GetAllUsers(c *gin.Context) {
	users := services.UsersService.GetAllUsers()
	utils.Respond(c, http.StatusOK, users)
}

// SaveUser Saves a new user
func SaveUser(c *gin.Context) {

	var user domain.User

	if err := c.ShouldBindJSON(&user); err != nil {
		apiErr := &utils.ApplicationError{
			Message:    "invalid json body",
			StatusCode: http.StatusBadRequest,
			Code:       "bad_request",
		}
		utils.RespondError(c, apiErr)
		return
	}

	savedUser := services.UsersService.SaveUser(user)

	utils.Respond(c, http.StatusCreated, savedUser)
}

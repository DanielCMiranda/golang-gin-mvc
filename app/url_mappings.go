package app

import "go-mvc/controllers"

func mapUrls() {
	router.GET("/users/:user_id", controllers.GetUser)
	router.GET("/users", controllers.GetAllUsers)
	router.POST("/users", controllers.SaveUser)
}

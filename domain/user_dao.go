package domain

import (
	"fmt"
	"math/rand"
	"net/http"

	"go-mvc/utils"
)

var (
	users = map[int64]*User{
		123: {Id: 123, FirstName: "Fede", LastName: "Leon", Email: "myemail@gmail.com"},
	}

	// UserDao will be abstraction to the database in the system
	UserDao userDaoInterface
)

func init() {
	UserDao = &userDao{}
}

type userDaoInterface interface {
	GetUser(int64) (*User, *utils.ApplicationError)
	GetAllUsers() []*User
	SaveUser(user User) *User
}

type userDao struct{}

func (u *userDao) GetUser(userID int64) (*User, *utils.ApplicationError) {

	if user := users[userID]; user != nil {
		return user, nil
	}

	return nil, &utils.ApplicationError{
		Message:    fmt.Sprintf("user %v does not exists", userID),
		StatusCode: http.StatusNotFound,
		Code:       "not_found",
	}
}

func (u *userDao) GetAllUsers() []*User {

	result := []*User{}
	for _, v := range users {
		result = append(result, v)
	}

	return result
}

func (u *userDao) SaveUser(user User) *User {
	user.Id = uint64(rand.Intn(99999999))
	users[int64(user.Id)] = &user
	return &user
}

### Dependencies

`go get -u github.com/gin-gonic/gin`


### Making requests to the application

`curl -X GET -H "Content-Type: application/json" http://127.0.0.1:8080/users/123`

`curl -X GET -H "Content-Type: application/json" http://127.0.0.1:8080/users`

```
curl -X POST -H "Content-Type: application/json" \
    http://127.0.0.1:8080/users \
    -d '{"firstName": "Fulano", "lastName": "de Tal", "email": "fulano@bla.com"}'
```
